<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Http\Requests\ServiceRequest;
use App\Models\Service;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ServiceController extends Controller
{
    use UploadTrait;

    public function index()
    {

        return view('dashboard.services.index');
    }


    public function create()
    {
        return view('dashboard.services.create');
    }

    public function data()
    {
        $model = 'services';
        $Services = Service::with('photo')->get();
        return DataTables::of($Services)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addColumn('photo', function ($raw) {
                return '<img width="100" height="80"  src="' . $raw->image . '" >';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['photo' => 'photo', 'check_item' => 'check_item'])
            ->make(true);

    }//end of data function


    public function store(ServiceRequest $request)
    {


        $data = $request->validated();
        unset($data['photo']);


        $Service = Service::create($data);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'services');
            $Service->photo()->create([
                'src' => $photo,
                'type' => 'Service',
            ]);
        };

        return redirect()->route('services.index')->with('success', 'تم إضافه البيانات بنجاح');

    }


    public function edit(Service $service)
    {
        return view('dashboard.services.edit', compact('service'));
    }

    public function update(ServiceRequest $request, Service $service)
    {

        $data = $request->validated();

        unset($data['photo']);
        $service->update($data);
        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'services');
            $service->photo ? $this->deleteOldPhoto('images/services/', $service->photo->src)
                & $service->photo()->update(['src' => $photo]) : $service->photo()->create(['src' => $photo, 'type' => 'service']);
        }

        return redirect()->route('services.index')->with('success', 'تم تعديل البيانات بنجاح');


    }


    public function destroy(Service $service)
    {
        $service->photo ? $this->deleteOldPhoto('images/services/', $service->photo->src) & $service->photo()->delete() : '';
        Service::destroy($service->id);
        return redirect()->route('services.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $service = Service::findOrFail($item);
            $this->destroy($service);
        }
        Service::destroy($request->items);
        return redirect()->route('services.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function
}
