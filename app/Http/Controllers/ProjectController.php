<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Http\Requests\ProjectRequest;
use App\Models\Project;
use App\Models\Umra;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProjectController extends Controller
{
    use UploadTrait;

    public function index()
    {

        return view('dashboard.projects.index');
    }


    public function create()
    {
        return view('dashboard.projects.create');
    }

    public function data()
    {
        $model = 'projects';
        $projects = Project::with('photos')->get();
        return DataTables::of($projects)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['check_item' => 'check_item'])
            ->make(true);

    }//end of data function


    public function store(ProjectRequest $request)
    {

        $data = $request->validated();

        unset($data['photos']);


        $project = Project::create($data);

        if ($request->hasFile('photos')) {
            $photos = $this->uploadAlbum($request->photos, 'projects');

            foreach ($photos as $photo) {

                $project->photos()->create([
                    'src' => $photo,
                    'type' => 'project',
                ]);
            }
        };

        return redirect()->route('projects.index')->with('success', 'تم إضافه البيانات بنجاح');

    }


    public function edit(Project $project)
    {
        return view('dashboard.projects.edit', compact('project'));
    }

    public function update(ProjectRequest $request, Project $project)
    {

        $data = $request->validated();
        unset($data['photos']);
        $project->update($data);
        if (!empty($request->old)) {
            $this->deleteOldAlbum('images/projects/', $project->photos, $request->old);
        } else {
            // delete all old photos
            $this->deleteAllOLdPhotos('images/projects', $project->photos);
        }
        if ($request->hasFile('photos')) {
            $photos = $this->uploadAlbum($request->photos, 'projects');

            foreach ($photos as $photo) {
                $project->photos()->create([
                    'src' => $photo,
                    'type' => 'project',
                ]);
            }
        }

        return redirect()->route('projects.index')->with('success', 'تم تعديل البيانات بنجاح');
    }


    public function destroy(Project $project)
    {
        $project->photos ? $this->deleteAlbum('images/projects/', $project->photos) & $project->photos()->delete() : '';
        Project::destroy($project->id);
        return redirect()->route('projects.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $Umra = Project::findOrFail($item);
            $this->destroy($Umra);
        }
        Project::destroy($request->items);
        return redirect()->route('projects.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function

    public function deleteAlbum($path, $files)
    {

        foreach ($files as $file) {
            $this->deleteOldPhoto($path, $file->src);
        }

    }//end of deleteAlbum function
}
