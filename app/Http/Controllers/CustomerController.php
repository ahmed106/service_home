<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CustomerController extends Controller
{
    use UploadTrait;

    public function index()
    {

        return view('dashboard.customers.index');
    }


    public function create()
    {
        return view('dashboard.customers.create');
    }

    public function data()
    {
        $model = 'customers';
        $Customers = Customer::with('photo')->get();
        return DataTables::of($Customers)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addColumn('photo', function ($raw) {
                return '<img width="100" height="80"  src="' . $raw->image . '" >';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['photo' => 'photo', 'check_item' => 'check_item'])
            ->make(true);

    }//end of data function


    public function store(CustomerRequest $request)
    {

        $data = $request->validated();
        unset($data['photo']);


        $Customer = Customer::create($data);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'customers');
            $Customer->photo()->create([
                'src' => $photo,
                'type' => 'customer',
            ]);
        };

        return redirect()->route('customers.index')->with('success', 'تم إضافه البيانات بنجاح');

    }


    public function edit(Customer $customer)
    {
        return view('dashboard.customers.edit', compact('customer'));
    }

    public function update(CustomerRequest $request, Customer $customer)
    {

        $data = $request->validated();
        unset($data['photo']);

        $customer->update($data);
        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'customers');
            $customer->photo ? $this->deleteOldPhoto('images/customers/', $customer->photo->src)
                & $customer->photo()->update(['src' => $photo]) : $customer->photo()->create(['src' => $photo, 'type' => 'Customer']);
        }

        return redirect()->route('customers.index')->with('success', 'تم تعديل البيانات بنجاح');


    }


    public function destroy(Customer $customer)
    {
        $customer->photo ? $this->deleteOldPhoto('images/customers/', $customer->photo->src) & $customer->photo()->delete() : '';
        Customer::destroy($customer->id);
        return redirect()->route('customers.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $customer = Customer::findOrFail($item);
            $this->destroy($customer);
        }
        Customer::destroy($request->items);
        return redirect()->route('customers.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function
}
