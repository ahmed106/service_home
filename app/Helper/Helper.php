<?php

if (!function_exists('active')) {
    function active($url)
    {
        if (request()->segment(1) == $url) {
            return 'active';
        } elseif (request()->segment(2) == $url) {
            return 'active';
        } else {
            return '';
        }
    }
};

if (!function_exists('getProjectAlbum')) {
    function getProjectAlbum($id)
    {
        $project = \App\Models\Project::findOrFail($id);
        if (count($project->photos) > 0) {
            foreach ($project->photos as $photo) {
                $photos [] = [
                    'id' => $photo->id,
                    'src' => asset('images/projects/' . $photo->src)
                ];
            }
        } else {
            $photos = [];
        }
        return json_encode($photos);


    }
}
