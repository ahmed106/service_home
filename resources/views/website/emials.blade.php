@component('mail::message')


<h4>{{$data['name']}}</h4>
<span>{{$data['email']}}</span>
<p style="">{{$data['phone']}}</p>
<p>{{$data['body']}}</p>
Thanks,<br>
{{ $setting->website_name }}
@endcomponent
