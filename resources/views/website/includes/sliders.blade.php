<p class="rs-p-wp-fix"></p>

<rs-module-wrap id="rev_slider_1_1_wrapper" class="ttm-slider-wrapper" data-alias="main-classic-01"
                data-source="gallery"
                style="visibility:hidden;background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
    <rs-module id="rev_slider_1_1" data-version="6.5.7">
        <rs-slides>
            @if($sliders->count()>0)
                @foreach($sliders as $index=> $slider)
                    <rs-slide data-links="{{$slider->link}}" style="position: absolute;" data-key="rs-{{$index}}" data-title="Slide"
                              data-thumb="{{asset('assets/website')}}/revolution/assets/mainslider-bg1-50x100.jpg" data-filter="e:late;b:6;"
                              data-in="o:0;sx:1.05;sy:1.05;e:power1.in;">
                        <img src="{{asset('assets/website')}}/revolution/assets/mainslider-bg1-50x100.jpg" title="mainslider-bg1" width="1920"
                             height="810" class="rev-slidebg tp-rs-img rs-lazyload"
                             data-lazyload="{{$slider->image}}" data-no-retina>
                             <div class="desc-slider"> 
                                 <span class="right"></span>
                                    <h3>{{$slider->title}}</h3>
                                    <p>{{$slider->content}}</p>
                                    <span class="left"></span>
                             </div>
                    </rs-slide>
                @endforeach
            @endif
        </rs-slides>
    </rs-module>
</rs-module-wrap>





@push('js')
    <script>
        $('rs-slide').on('click', function (e) {
            let link = $(this).data('links');

            window.open(link);

        })
    </script>

@endpush

