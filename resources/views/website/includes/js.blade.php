<!-- Javascript -->
<script src="{{asset('assets/website')}}/js/jquery-3.6.0.min.js"></script>
<script src="{{asset('assets/website')}}/js/jquery-migrate-3.3.2.min.js"></script>
<script src="{{asset('assets/website')}}/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('assets/website')}}/js/jquery.easing.js"></script>
<script src="{{asset('assets/website')}}/js/jquery-waypoints.js"></script>
<script src="{{asset('assets/website')}}/js/jquery-validate.js"></script>
<script src="{{asset('assets/website')}}/js/jquery.prettyPhoto.js"></script>
<script src="{{asset('assets/website')}}/js/slick.min.js"></script>
<script src="{{asset('assets/website')}}/js/numinate.min.js"></script>
<script src="{{asset('assets/website')}}/js/imagesloaded.min.js"></script>
<script src="{{asset('assets/website')}}/js/jquery-isotope.js"></script>
<script src="{{asset('assets/website')}}/revolution/js/rbtools.min.js"></script>
<script src="{{asset('assets/website')}}/revolution/js/rs6.min.js"></script>
<script src="{{asset('assets/website')}}/revolution/js/slider.js"></script>
<script src="{{asset('assets/website')}}/js/main.js?v=1"></script>
<!-- Javascript end-->

<script src="{{asset('assets/dashboard/plugins/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/dashboard/plugins/sweet_alert/swal.js')}}"></script>
@include('dashboard.includes.swal')
@toastr_render
<script>
    $(window).load(function () {
        $("#preloader").fadeOut()
    })
</script>
