<link rel="shortcut icon" href="{{asset('assets/website')}}/images/logo-img.svg"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/website')}}/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/website')}}/revolution/css/rs6.css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/website')}}/css/shortcodes.css?v=1"/>
<!-- <link rel="stylesheet" type="text/css"href="{{asset('assets/website')}}/css/main.css" /> -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/website')}}/css/main-rtl.css?v=5"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/website')}}/css/megamenu.css"/>
<link rel="stylesheet" href="{{asset('assets/dashboard/plugins/toastr/toastr.css')}}">
@toastr_css
