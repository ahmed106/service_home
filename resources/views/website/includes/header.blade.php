<!--header start-->
<header id="masthead" class="header ttm-header-style-01">
    <!-- topbar -->
    <div class="top_bar ttm-bgcolor-darkgrey clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="top_bar_contact_item">
                                <div class="top_bar_icon"><i class="flaticon flaticon-placeholder"></i></div>
                                <div class="top_bar_content">{{$setting?$setting->website_address:''}}</div>
                            </div>
                            <div class="top_bar_contact_item">
                                <div class="top_bar_icon"><i class="flaticon flaticon-envelope"></i></div>
                                <div class="top_bar_content"><a
                                        href="mailto:{{$setting?$setting->website_email:''}}">{{$setting?$setting->website_email:''}}</a></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="contact-info">
                                <div class="top_bar_contact_item top_bar_social ml-auto p-0">
                                    <ul class="social-icons d-flex">
                                        <li>
                                            <a class="ttm-social-facebook" href="{{$setting?$setting->website_facebook:''}}" rel="noopener"
                                               aria-label="facebook"><i class=" fa fa-facebook-f"></i></a></li>
                                        <li>
                                            <a class="ttm-social-twitter" href="{{$setting?$setting->website_twitter:''}}" rel="noopener"
                                               aria-label="twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li>
                                            <a class="ttm-social-linkedin" href="{{$setting?$setting->website_linked_in:''}}" rel="noopener"
                                               aria-label="google"><i class="fa fa-linkedin"></i></a></li>

                                    </ul>
                                </div>
                                <div class="top_bar_contact_item ttm-highlight-right">
                                    <div class="top_bar_icon"><i class="flaticon-phone-call"></i></div>
                                    <div class="top_bar_content"> تواصل معنا :
                                        <strong><a href="tel:{{$setting?$setting->website_phone:''}}">{{$setting?$setting->website_phone:''}}</a></strong></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- topbar end -->
    <!-- site-header-menu -->
    <div id="site-header-menu" class="site-header-menu ttm-bgcolor-white">
        <div class="site-header-menu-inner ttm-stickable-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 ">
                        <!--site-navigation -->
                        <div
                            class="site-navigation d-flex flex-row  justify-content-between align-items-center">
                            <!-- site-branding -->
                            <div class="site-branding ">
                                <a class="home-link" href="{{url('/')}}" title="Codlop" rel="home">
                                    @if($setting)
                                        <img id="logo-img" width="170" class="img-fluid auto_size" src="{{$setting->image}}" alt="logo-img">
                                    @endif
                                </a>
                            </div><!-- site-branding end -->
                            <div class=" d-flex flex-row mg-a-h ">
                                <div class="btn-show-menu-mobile menubar menubar--squeeze">
                                            <span class="menubar-box">
                                                <span class="menubar-inner"></span>
                                            </span>
                                </div>
                                <!-- menu -->
                                <nav class="main-menu menu-mobile" id="menu">
                                    <ul class="menu">

                                        @foreach($pages->take(6) as $page)

                                            @if($page->route !=='services')
                                                <li class="mega-menu-item  {{active($page->route)}} {{$page->route == 'home' & request()->route()->getName() == 'home'?'active':''}} ">
                                                    <a href="{{url($page->url)}}">{{$page->name}}</a>
                                                </li>
                                            @else
                                                <li class="mega-menu-item {{active('services')}}">
                                                    <a href="{{url($page->url)}}" class="mega-menu-link">{{$page->name}}</a>
                                                    <ul class="mega-submenu">

                                                        @if($services->count()>0)
                                                            @foreach($services->take(5) as $service)
                                                                <li><a href="{{url('services/'.$service->id)}}">{{$service->title}}</a></li>
                                                            @endforeach
                                                        @endif


                                                    </ul>
                                                </li>
                                            @endif

                                        @endforeach

                                    </ul>
                                </nav><!-- menu end -->

                            </div><!-- site-navigation end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- site-header-menu end-->
</header>
<!--header end-->
