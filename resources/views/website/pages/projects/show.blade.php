@extends('website.layouts.master')
@push('title')
    {{$page_name}} |     {{$project->title}}
@endpush
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/website')}}/js/bxslider/css/jquery.bxslider.css"/>

@endpush
@section('content')

    <!-- page-title -->
    <div class="ttm-page-title-row ttm-bg ttm-bgimage-yes ttm-bgcolor-dark clearfix">
        <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="ttm-page-title-row-inner">
                        <div class="page-title-heading">
                            <h2 class="title">{{$page_name}}</h2>
                        </div>
                        <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{url('')}}">
                                        <i class="themifyicon ti-home"></i> &nbsp;
                                        الرئيسية</a>
                                </span>
                            <span>{{$page_name}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-title end -->
    <!--site-main start-->
    <div class="site-main">
        <!-- Project-style-section -->
        <section class="ttm-row project-single-section clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ttm-pf-single-content-wrapper">
                            <div class="ttm-pf-single-content-wrapper-innerbox">
                                <div class="ttm-pf-detail-box">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul class="bxslider">
                                                @foreach($project->photos as $photo)
                                                    <li><img src="{{asset('images/projects/'.$photo->src)}}" alt=""></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="ttm-pf-single-content-area">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="flex-div">
                                                <h3 class="">


                                                    اسم العميل
                                                    <span>{{$project->customer}}</span>
                                                </h3>
                                                <h3 class="">
                                                    تاريخ الانتهاء
                                                    <span>{{$project->end_date}}</span>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="ttm-horizontal_sep width-100 margin_top20 margin_bottom20"></div>
                                        <div class="col-lg-12">
                                            <div>
                                                <h2 class="res-991-padding_top20">{{$project->title}}</h2>
                                                <p>
                                                    {!! $project->content !!}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ttm-horizontal_sep width-100 margin_top20 margin_bottom20"></div>
                                </div>
                                <div class="row ttm-pf-single-related-wrapper margin_top50 mb_15">
                                    <div class="col-12">
                                        <h2>أعمال أخري</h2>
                                    </div>
                                    @foreach($projects as $project)
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <!-- featured-imagebox-portfolio -->
                                            <div class="featured-imagebox featured-imagebox-portfolio style1">
                                                <!-- ttm-box-view-overlay -->
                                                <div class="ttm-box-view-overlay ttm-portfolio-box-view-overlay">
                                                    <!-- featured-thumbnail -->
                                                    <div class="featured-thumbnail">
                                                        <a href="{{url('projects/'.$project->id)}}"> <img class="img-fluid" src="{{asset('images/projects/'.$project->photos->first()->src)}}" alt="image"></a>
                                                    </div><!-- featured-thumbnail end-->
                                                    <div class="ttm-media-link">
                                                        <a href="{{url('projects/'.$project->id)}}" class="ttm_link"><i
                                                                class="ti ti-plus"></i></a>
                                                    </div>
                                                </div><!-- ttm-box-view-overlay end-->
                                                <div class="featured-content featured-content-portfolio">
                                                    <div class="featured-title">
                                                        <h3><a href="{{url('projects/'.$project->id)}}">{{$project->title}}</a></h3>
                                                    </div>
                                                </div>
                                            </div><!-- featured-imagebox-portfolio -->
                                        </div>
                                    @endforeach


                                </div><!-- row end-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Project-style-section-end-->
    </div>
    <!--site-main end-->
@endsection
@push('js')
    <script src="{{asset('assets/website')}}/js/bxslider/js/jquery.bxslider.js"></script>

@endpush
