@extends('website.layouts.master')
@push('title')
    الرئيسيه
@endpush

@section('content')
    <!-- Banner -->
    @include('website.includes.sliders')

    <div class="site-main">
        <!--top-section-->
        <section class="ttm-row top-section clearfix">
            <div class="container">
                <div class="row ">
                    <div class="col-lg-12">
                        <div class="row mt_160 res-991-margin_top_0 g-0">
                            <div class="col-lg-6">
                                <div
                                    class="ttm-bgcolor-darkgrey padding_top40 padding_left50 padding_right50 padding_bottom60 res-320-padding_top30 res-320-padding_bottom30 res-320-padding_lr15 margin_left30 margin_right30 position-relative column-title">
                                    <!--col-title -->
                                    <div class="col-title section-title">
                                        <h2 class="ttm-textcolor-whitecolor">
                                            {{$about?$about->title:''}}
                                        </h2>
                                        <p>
                                            {!! \Illuminate\Support\Str::limit($about?$about->vision:'',150) !!}
                                            &nbsp;<a class="ttm-textcolor-skincolor" href="{{url('about')}}">قراءة المزيد</a>
                                        </p>
                                    </div>
                                    <!--col-title-end -->
                                    <div class=" padding_top30 mb_120 res-575-margin_bottom0 ">
                                        <img class="img-fluid" src="{{asset('assets/website')}}/images/single-img1.jpg" alt="single-img1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    @if($services->count()>0)
                                        @foreach($services->take(2) as $service)
                                            <div class="col-sm-6 col-12">
                                                <div class="featured-icon-box icon-align-top-content style1">
                                                    <div class="featured-icon">
                                                        <div
                                                            class="ttm-icon ttm-icon_element-onlytxt ttm-icon_element-color-skincolor ttm-icon_element-size-md">
                                                            <i class="flaticon {{$service->icon}}"></i>
                                                        </div>
                                                    </div>
                                                    <div class="featured-content">
                                                        <div class="featured-title">
                                                            <h3>{{$service->title}}</h3>
                                                        </div>
                                                        <div class="featured-desc">
                                                            <p>{!! $service->content !!}</p>
                                                        </div>
                                                        <a class="ttm-btn btn-inline ttm-btn-size-md ttm-icon-btn-right ttm-textcolor-darkgreycolor"
                                                           href="{{url('services/'.$service->id)}}">قراءة المزيد<i
                                                                class="fa fa-long-arrow-left"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                    @endif


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--top-section-end-->
        <!-- about-section -->
        <section class="ttm-row Lifestyle-section clearfix ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="padding_top20 ">
                            <div class="section-title">
                                <div class="title-header">
                                    <h3>عن الشركة</h3>
                                    <h2 class="title">{{$about?$about->title:''}} </h2>
                                </div>
                                <div class="title-desc about-desc">
                                    <p>{!! $about?$about->content:'' !!}</p>

                                </div>
                            </div>
                            <div class="margin_top20">
                                <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-border ttm-btn-color-dark margin_right20 margin_top15"
                                   href="{{url('about')}}">اعرف المزيد <i class="fa fa-long-arrow-left"></i></a>
                                <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-btn-color-skincolor  margin_top15"
                                   href="{{url('services')}}">خدماتنا
                                    <i class="fa fa-long-arrow-left"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div
                            class="ttm_single_image-wrapper text-center imagestyle-one res-1199-padding_top70 mt-50 ">
                            <img class="img-fluid auto_size" src="{{$about?$about->image:''}}" alt="single-02"
                                 height="412" width="512">
                            <div class="ttm_single_image_text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--about-section-end-->
        <!--service-section-->
        <section class="ttm-row service-one-section ttm-bgimage-yes bg-img1 ttm-bg ttm-bgcolor-darkgrey clearfix">
            <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--section-title -->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h3>افضل خدماتنا</h3>
                                <h2 class="title"><span>خدمات</span> ذات جودة ثابتة</h2>
                            </div>
                        </div>
                        <!--section-title-end -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            @if($services->count()>0)
                                @foreach($services->take(4) as $service)

                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="featured-icon-box icon-align-top-content style2">
                                            <div class="featured-icon">
                                                <div

                                                    class="ttm-icon ttm-icon_element-onlytxt ttm-icon_element-size-md ttm-icon_element-color-skincolor">
                                                    <i class="flaticon {{$service->icon}}"></i>
                                                </div>
                                            </div>
                                            <div class="featured-content">
                                                <div class="featured-title">
                                                    <h3>{{$service->title}}</h3>
                                                </div>
                                                <div class="featured-desc">
                                                    <p>
                                                        {!! $service->content !!}
                                                    </p>
                                                </div>
                                                <a class="ttm-btn btn-inline ttm-btn-size-md ttm-icon-btn-right ttm-btn-color-skincolor"
                                                   href="{{url('services/'.$service->id)}}">قراءة المزيد<i
                                                        class="fa fa-long-arrow-left"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif


                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--service-section-end-->
        <!--portfolio-section-->
        <section class="ttm-row portfolio-section clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ttm-tabs ttm-tab-style-03">
                            <div class="d-lg-flex align-items-center justify-content-between">
                                <div
                                    class="ttm-bg ttm-col-bgcolor-yes ttm-bgcolor-grey ttm-bg ttm-left-span res-991-padding_top50 res-991-padding_right30  res-991-padding_bottom30 res-991-mt_40  res-991-mr_20">
                                    <div class="ttm-col-wrapper-bg-layer ttm-bg-layer spacing-1">
                                        <div class="ttm-col-wrapper-bg-layer-inner"></div>
                                    </div>
                                    <div class="layer-content">
                                        <!-- section title -->
                                        <div class="section-title margin_bottom45">
                                            <div class="title-header">
                                                <h3>عن مشاريعنا</h3>
                                                <h2 class="title">اخر <span>اعمالنا</span></h2>
                                            </div>
                                        </div><!-- section title end -->
                                    </div>
                                </div>
                            </div>
                            <div class="content-tab">
                                <!-- content-inner -->
                                <div class="row isotope-project">
                                    @if($projects->count()>0)
                                        @foreach($projects->take(3) as $project)
                                            <div class="col-lg-4 col-md-4 col-sm-6 project_item">
                                                <!-- featured-imagebox-portfolio -->
                                                <div class="featured-imagebox featured-imagebox-portfolio style1">
                                                    <!-- ttm-box-view-overlay -->
                                                    <div class="ttm-box-view-overlay ttm-portfolio-box-view-overlay">
                                                        <!-- featured-thumbnail -->
                                                        <div class="featured-thumbnail">

                                                            <a href="{{url('projects/'.$project->id)}}"> <img class="img-fluid"
                                                                                                              src="{{asset('images/projects/'.$project->photos->first()->src)}}"
                                                                                                              alt="image"></a>
                                                        </div><!-- featured-thumbnail end-->
                                                        <div class="ttm-media-link">
                                                            <a href="{{url('projects/'.$project->id)}}" class="ttm_link"><i
                                                                    class="ti ti-plus"></i></a>
                                                        </div>
                                                    </div><!-- ttm-box-view-overlay end-->
                                                    <div class="featured-content featured-content-portfolio">
                                                        <div class="featured-title">
                                                            <h3><a href="{{url('projects/'.$project->id)}}">{{$project->title}}</a></h3>
                                                        </div>
                                                    </div>
                                                </div><!-- featured-imagebox-portfolio -->
                                            </div>
                                        @endforeach
                                    @endif


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--portfolio-section-end-->
        <!-- image-section-->
        @if($home_service_section)
            <section class="ttm-row image-section ttm-bgimage-yes bg-img3 ttm-bg ttm-bgcolor-dark clearfix" style="background-image: url({{$home_service_section->image}}) !important; ">
                <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- section title -->
                            <div class="section-title style3 ">
                                <h4 class="title border-rad_5">نحن هنا لمساعدتك</h4>
                                <h2>{{$home_service_section->title}}</h2>
                                <div class="title-desc">
                                    <p>{!! strip_tags($home_service_section->content) !!}</p>
                                </div>
                            </div><!-- section title end -->
                        </div>
                    </div>
                </div>
            </section>
        @endif


        @if($home_vision_section)
        <section class="ttm-row progressbar-section ttm-bg bg-layer-equal-height ttm-bgcolor-grey clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row g-0">
                            <div class="col-lg-6">
                                <div class="ttm-col-bgimage-yes ttm-bg col-bg-img-four ttm-left-span">
                                    <div class="ttm-col-wrapper-bg-layer ttm-bg-layer" style="background-image: url({{$home_vision_section->image}})!important"></div>
                                    <div class="layer-content"></div>
                                </div>
                                <!--col-image-four -->
                                <img class="img-fluid ttm-equal-height-image"
                                     src="{{asset('assets/website')}}/images/bg-image/col-bgimage-2.jpg" alt="bg-image">
                                <!--col-image-four -->
                            </div>
                            <div class="col-lg-6">
                                <div
                                    class="ttm-bg ttm-col-bgcolor-yes ttm-bgcolor-white ttm-bg ttm-right-span spacing-2 z-index-2">
                                    <div class="ttm-col-wrapper-bg-layer ttm-bg-layer">
                                        <div class="ttm-col-wrapper-bg-layer-inner"></div>
                                    </div>
                                    <div class="layer-content">
                                        <!-- section title -->
                                        <!-- section title -->
                                        <div class="section-title ">
                                            <div class="title-header">
                                                <h3>أداء عالي</h3>
                                                <h2 class="title"> {{$home_vision_section->title}}</h2>
                                            </div>
                                            <div class="title-desc">
                                                <p>{!! strip_tags($home_vision_section->content) !!}</p>
                                            </div>

                                        </div><!-- section title end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="margin_top90 padding_bottom30 res-991-margin_top50">
                    <div class="padding_left15 padding_right15">
                        <h3 class="title">عملائنا <span>الكرام</span></h3>
                    </div>
                    <div dir="rtl" class="row slick_slider"
                         data-slick='{"slidesToShow": 6, "slidesToScroll": 1, "arrows":false, "autoplay":false, "infinite":true, "responsive": [{"breakpoint":1200,"settings":{"slidesToShow": 5}}, {"breakpoint":1024,"settings":{"slidesToShow": 4}}, {"breakpoint":777,"settings":{"slidesToShow": 3}},{"breakpoint":575,"settings":{"slidesToShow": 2}},{"breakpoint":400,"settings":{"slidesToShow": 1}}]}'>
                        @if($customers->count()>0)
                            @foreach($customers as $customer)
                                <div class="col-lg-12">


                                    <div class="client-box style1">
                                        <div class="client-thumbnail">
                                            <img class="img-fluid auto_size" width="160" height="98"
                                                 src="{{$customer->image}}" alt="image">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif


                    </div><!-- row end -->
                </div>
            </div>
        </section>
        @endif
        <!-- progress-bar-section-end-->
        <!--Blog-section-->
        <section class="ttm-row blog-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- section title -->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h3>المقالات</h3>
                                <h2 class="title"> احدث الاخبار & <span>المقالات</span></h2>
                            </div>
                        </div><!-- section title end -->
                    </div>
                </div>
                <div dir="rtl" class="row slick_slider mb_15"
                     data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows":false, "dots":false, "autoplay":false, "infinite":true, "responsive": [{"breakpoint":1024,"settings":{"slidesToShow": 2}} , {"breakpoint":900,"settings":{"slidesToShow": 2}}, {"breakpoint":575,"settings":{"slidesToShow": 1}}]}'>
                    @if($blogs->count()>0)
                        @foreach($blogs as $blog)
                            <div class="col-lg-4">
                                <!-- featured-imagebox-post -->
                                <div class="featured-imagebox featured-imagebox-post style1">
                                <a href="{{url('blogs/'.$blog->id)}}" class="featured-thumbnail">
                                    <img class="img-fluid" src="{{$blog->image}}" alt="">
                                </a>
                                    <div class="featured-content">
                                        <!-- ttm-box-post-date -->
                                        <div class="ttm-box-post-date">
                                        <span class="ttm-entry-date">
                                            <time class="entry-date" datetime="2021-05-18T04:16:25+00:00">{{\Illuminate\Support\Carbon::create($blog->created_at)->day}}<span
                                                    class="entry-month entry-year">{{\Illuminate\Support\Carbon::create($blog->created_at)->monthName}}</span></time>
                                        </span>
                                        </div><!-- ttm-box-post-date end -->
                                        <div class="post-meta">
                                        <span class="ttm-meta-line byline">
                                            <i class="ti ti-user"></i> {{$blog->creator->name}} </span>
                                        </div>
                                        <div class="featured-title">
                                            <h3><a href="{{url('blogs/'.$blog->id)}}">
                                                    {{$blog->title}}

                                                </a></h3>
                                        </div>
                                        <div class="featured-desc">
                                            <p>
                                                {!! $blog->content !!}
                                            </p>
                                        </div>
                                        <a class="ttm-btn btn-inline ttm-btn-size-md ttm-icon-btn-right ttm-textcolor-darkgreycolor"
                                           href="{{url('blogs/'.$blog->id)}}">قراءة المزيد<i class="fa fa-long-arrow-left"></i></a>
                                    </div>
                                </div><!-- featured-imagebox-post end -->
                            </div>
                        @endforeach
                    @endif


                </div>
            </div>
        </section>
        <!--Blog-section-end-->
    </div>
@endsection
