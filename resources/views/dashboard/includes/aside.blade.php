<!-- Sidemenu -->
<div class="main-sidebar main-sidebar-sticky side-menu">
    <div class="sidemenu-logo">
        <a class="main-logo" href="{{route('dashboard.index')}}">
            <img src="{{asset('assets/dashboard')}}/img/logo.svg" class="header-brand-img" alt="logo">
        </a>
    </div>
    <div class="main-sidebar-body">
        <ul class="nav">
            <li class="nav-item {{request()->route()->getName() =='dashboard.index' ? 'active':''}}">
                <a class="nav-link" href="{{url('/dashboard')}}">
                    <i class="fa fa-home sidemenu-icon"></i>
                    <span class="sidemenu-label">الرئيسية</span></a>
            </li>
            <li class="nav-item {{active('sliders')}}">
                <a class="nav-link" href="{{route('sliders.index')}}">
                    <i class="fa fa-image sidemenu-icon"></i>
                    <span class="sidemenu-label">الاسلايدر</span></a>
            </li>

            <li class="nav-item {{active('pages')}}">
                <a class="nav-link" href="{{route('pages.index')}}">
                    <i class="fa fa-hand-paper-o sidemenu-icon"></i>
                    <span class="sidemenu-label">الصفحات الرئيسيه</span></a>
            </li>
            <li class="nav-item {{active('about')}}">
                <a class="nav-link" href="{{route('about.index')}}">
                    <i class="fa fa-info sidemenu-icon"></i>
                    <span class="sidemenu-label">من نحن</span></a>
            </li>
            <li class="nav-item {{active('customers')}}">
                <a class="nav-link" href="{{route('customers.index')}}">
                    <i class="fa fa-users sidemenu-icon"></i>
                    <span class="sidemenu-label">عملائنا</span></a>
            </li>
            <li class="nav-item {{active('blogs')}}">
                <a class="nav-link" href="{{route('blogs.index')}}">
                    <i class="fa fa-newspaper sidemenu-icon"></i>
                    <span class="sidemenu-label">المقالات</span></a>
            </li>
            <li class="nav-item {{active('services')}}">
                <a class="nav-link" href="{{route('services.index')}}">
                    <i class="fa fa-list sidemenu-icon"></i>
                    <span class="sidemenu-label">خدماتنا</span></a>
            </li>
            <li class="nav-item {{active('projects')}}">
                <a class="nav-link" href="{{route('projects.index')}}">
                    <i class="fa fa-project-diagram sidemenu-icon"></i>
                    <span class="sidemenu-label">مشاريعنا</span></a>
            </li>

            <li class="nav-item {{active('sections')}}">
                <a class="nav-link" href="{{route('sections.index')}}">
                    <i class="fa fa-project-diagram sidemenu-icon"></i>
                    <span class="sidemenu-label">السكاشن الإضافيه</span></a>
            </li>
            <li class="nav-item {{active('contact-us')}}">
                <a class="nav-link" href="{{route('contact-us.index')}}">
                    <i class="fa fa-phone sidemenu-icon"></i>
                    <span class="sidemenu-label">تواصل معنا</span></a>
            </li>
            <li class="nav-item {{active('settings')}}">
                <a class="nav-link" href="{{route('settings.index')}}">
                    <i class="fa fa-cog sidemenu-icon"></i>
                    <span class="sidemenu-label">اعدادات الموقع</span></a>
            </li>
        </ul>
    </div>
</div> <!-- End Sidemenu -->
