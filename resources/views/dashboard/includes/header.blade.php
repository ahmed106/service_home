<!-- Main Header-->
<div class="main-header side-header sticky sticky-pin">
    <div class="container-fluid">
        <div class="main-header-left"><a class="main-header-menu-icon" href="#"
                                         id="mainSidebarToggle"><span></span></a></div>
        <div class="main-header-center">
            <div class="responsive-logo"><a href="{{route('dashboard.index')}}">
                    <img src="{{asset('assets/dashboard')}}/img/logo.svg" class="mobile-logo" alt="logo"></a>
            </div>
        </div>
        <div class="main-header-right">
            <div class="dropdown d-md-flex"><a class="nav-link icon full-screen-link" href=""> <i
                        class="fe fe-maximize fullscreen-button fullscreen header-icons"></i> <i
                        class="fe fe-minimize fullscreen-button exit-fullscreen header-icons"></i> </a></div>

            <div class="dropdown main-profile-menu">
                <a class="d-flex" href="">
                            <span class="main-img-user">
                                <img alt="avatar" src="{{asset('images/profile/'.auth()->user()->photo)}}">
                            </span>
                </a>
                <div class="dropdown-menu">
                    <div class="header-navheading">
                        <h6 class="main-notification-title">{{auth()->user()->name}}</h6>

                    </div>

                    <a class="dropdown-item" href="{{route('user.profile')}}"> <i class="fe fe-user"></i> الصفحه الشخصيه</a>
                    <a class="dropdown-item" href="javascript:void();" onclick="$('#logout_form').submit()"> <i class="fe fe-power"></i> تسجيل خروج </a>

                    <form id="logout_form" action="{{route('logout')}}" method="post">
                        @csrf
                    </form>
                </div>
            </div>
            <!-- Navresponsive closed -->
        </div>
    </div>
</div>
