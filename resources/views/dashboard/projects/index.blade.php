@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">
                <div class="card">
                    <div class="card-header">
                        <h3>مشاريعنا</h3>
                        <div class="flex-btns">
                            <a href="" id="delete_all_btn" class="btn ripple btn-danger d-none"><i class="fe fe-trash"></i> حذف الكل </a>
                            <a href="{{route('projects.create')}}" class="btn ripple btn-primary"><i class="fe fe-plus"></i> اضافة </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form id="bulk_delete" action="{{route('projects.bulkDelete')}}" method="post">
                                @csrf
                                <table class="table datatable table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" id="check_all">
                                        </th>
                                        <th>#</th>
                                        <th>العنوان</th>
                                        <th>العمليات</th>
                                    </tr>
                                    </thead>


                                    <tbody>


                                    </tbody>

                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->

@endsection
@push('js')
    <script>
        var table = $('.table.datatable').DataTable({
            lengthChange: true,

            "processing": true,
            "serverSide": true,
            "ajax": {
                url: '{{route('projects.data')}}',

            },
            "columns": [
                {data: 'check_item', name: 'check_item', sortable: false, searchable: false},
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'actions', name: 'actions'}
            ]


        });

    </script>
@endpush
