@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('services.update',$service->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <h3>تعديل خدمة</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$service->title}}" type="text" name="title" class="form-control">
                                        <span class="input-span">العنوان</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <textarea name="content" rows="3" class="form-control editor">{{$service->content}}</textarea>
                                        <span class="input-span">المحتوي</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <select name="icon" class="form-control" id="">
                                            <option {{$service->icon =='flaticon-construction'?'selected':''}} value="flaticon-construction">flaticon construction</option>
                                            <option {{$service->icon =='flaticon-house'?'selected':''}} value="flaticon-house">flaticon house</option>
                                            <option {{$service->icon =='flaticon-blueprint-1'?'selected':''}} value="flaticon-blueprint-1">flaticon blueprint 1</option>
                                            <option {{$service->icon =='flaticon-workers'?'selected':''}} value="flaticon-workers">flaticon workers</option>
                                            <option {{$service->icon =='flaticon-mixer-truck'?'selected':''}} value="flaticon-mixer-truck">flaticon mixer-truck</option>
                                            <option {{$service->icon =='flaticon-building-1'?'selected':''}} value="flaticon-building-1">flaticon building 1</option>
                                        </select>
                                        <span class="input-span">الأيقون</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <i class="icon-prev flaticon flaticon-blueprint-1"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <label class="">الصورة ( مقاس الصورة -- 670 طول * 770 عرض )</label>
                                        <input id="photo" type="file" name="photo">
                                        <img width="100" id="preview" height="100" src="{{$service->image}}" alt="">
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-container">
                                <div class="set">
                                    <a href="#">
                                        بيانات السيو SEO
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">
                                            <div class="input-custom">
                                                <input value="{{$service->meta_title}}" type="text" name="meta_title" class="form-control">
                                                <span class="input-span">عنوان الميتا</span>
                                            </div>
                                            <div class="input-custom">
                                                <input value="{{$service->meta_keywords}}" type="text" name="meta_keywords" class="form-control">
                                                <span class="input-span">الكلمات الدلالية</span>
                                            </div>
                                            <div class="input-custom">
                                                <textarea name="meta_description" class="form-control editor">{!! $service->meta_description !!}</textarea>
                                                <span class="input-span">وصف الميتا</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
