@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('sliders.update',$slider->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <h3>تعديل سلايدر</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$slider->title}}" type="text" name="title" class="form-control">
                                        <span class="input-span">العنوان</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$slider->link}}" type="url" name="link" class="form-control">
                                        <span class="input-span">الرابط</span>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$slider->content}}" type="text" name="content" class="form-control">
                                        <span class="input-span">المحتوي</span>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <label for="">الصورة ( مقاس الصورة -- 750 طول * 1660 عرض )</label>
                                        <input id="photo" type="file" name="photo">
                                        <img width="100" id="preview" height="100" src="{{$slider->image}}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-container">
                                <div class="set">
                                    <a href="#">
                                        بيانات السيو SEO
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">
                                            <div class="input-custom">
                                                <input value="{{$slider->meta_title}}" type="text" name="meta_title" class="form-control">
                                                <span class="input-span">عنوان الميتا</span>
                                            </div>
                                            <div class="input-custom">
                                                <input value="{{$slider->meta_keywords}}" type="text" name="meta_keywords" class="form-control">
                                                <span class="input-span">الكلمات الدلالية</span>
                                            </div>
                                            <div class="input-custom">
                                                <textarea name="meta_description" class="form-control editor">{!! $slider->meta_description !!}</textarea>
                                                <span class="input-span">وصف الميتا</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
