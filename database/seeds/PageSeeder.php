<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       $pages = [
           0=>[
                'name'=>'الرئيسيه',
                'route'=>'home',
               'url'=>'/',
           ],
           1=>[
               'name'=>'من نحن',
               'route'=>'about',
               'url'=>'/about',
           ],
           2=>[
               'name'=>'الخدمات',
               'route'=>'services',
               'url'=>'/services',
           ],
           3=>[
               'name'=>'المشاريع',
               'route'=>'projects',
               'url'=>'/projects',
           ],
           4=>[
               'name'=>'المقالات',
               'route'=>'blogs',
               'url'=>'/blogs',
           ],
           5=>[
               'name'=>'تواصل معنا',
               'route'=>'contact-us',
               'url'=>'/contact-us',
           ],
       ];

        foreach ($pages as $page) {
               \App\Models\Page::create($page);
       }

    }
}
