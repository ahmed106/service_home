<?php

return [
    // Limit the number of displayed toasts, by default no limits
    'maxItems' => null,

    'options' => [
        'closeButton' => true,
        'closeClass' => 'toast-close-button',
        'closeDuration' => 300,
        'closeEasing' => 'swing',
        'closeHtml' => '<button><i class="icon-off"></i></button>',
        'closeMethod' => 'fadeOut',
        'closeOnHover' => true,
        'containerId' => 'toast-container',
        'debug' => false,
        'escapeHtml' => false,
        'extendedTimeOut' => 500,
        'hideDuration' => 500,
        'hideEasing' => 'linear',
        'hideMethod' => 'fadeOut',
        'iconClass' => 'toast-info',
        'iconClasses' => [
            'error' => 'toast-error',
            'info' => 'toast-info',
            'success' => 'toast-success',
            'warning' => 'toast-warning',
        ],
        'messageClass' => 'toast-message',
        'newestOnTop' => false,
        'onHidden' => null,
        'onShown' => null,
        'positionClass' => 'toast-top-left',
        'preventDuplicates' => true,
        'progressBar' => true,
        'progressClass' => 'toast-progress',
        'rtl' => true,
        'showDuration' => 300,
        'showEasing' => 'swing',
        'showMethod' => 'fadeIn',
        'tapToDismiss' => true,
        'target' => 'body',
        'timeOut' => 3000,
        'titleClass' => 'toast-title',
        'toastClass' => 'toast',
    ],
];
